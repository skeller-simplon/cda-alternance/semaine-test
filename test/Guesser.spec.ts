import { Guesser } from "../src/Guesser";



describe('class Guesser', () => {

    let instance: Guesser;

    beforeEach(() => {
        instance = new Guesser();
    });

    test.each([
        [3, 3, "yes"]
    ])('.gee(%i, %i, "+")', (guess, toGuess, result) => {
        let guesser = new Guesser(toGuess);
        expect(guesser.checkNumberMatch(guess)).toEqual(result);
    });


});