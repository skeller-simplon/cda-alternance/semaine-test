import { CellValues, MineSweeper } from "../../src/game/MineSweeper";

describe('class MineSweeper', () => {

    let instance:MineSweeper;
    let testGrid:CellValues[][];
    beforeEach(() => {
        instance = new MineSweeper();
        testGrid = [
            [1,0,0,0,0,0,0,0,0,0],
            [0,1,0,0,0,0,0,0,0,0],
            [0,0,1,0,0,0,0,0,0,0],
            [0,0,0,1,0,0,0,0,0,0],
            [0,0,0,1,0,0,0,0,0,0],
            [0,0,0,1,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,1],
            [0,0,0,0,0,0,0,0,0,1],
            [0,0,0,1,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,1],
        ];
    });


    it('should return game grid', () => {

        const grid = instance.grid;

        expect(grid.length).toBe(10);
        expect(grid[0].length).toBe(10);

    });

    it('should contain mines', () => {

        const flatGrid = instance.grid.flat()

        expect(flatGrid).toContain(1);
    });

    it('should contain randomly placed mines', () => {

        const fakeRadom = jest.fn();

        for(let x = 0; x < 10; x++) {
            fakeRadom.mockReturnValueOnce(x/10);
            fakeRadom.mockReturnValueOnce(x/10);
            
        }
        const original = Math.random;
        Math.random = fakeRadom;

        const localInstance = new MineSweeper();

        const grid = localInstance.grid;

        expect(grid).toEqual([
            [1,0,0,0,0,0,0,0,0,0],
            [0,1,0,0,0,0,0,0,0,0],
            [0,0,1,0,0,0,0,0,0,0],
            [0,0,0,1,0,0,0,0,0,0],
            [0,0,0,0,1,0,0,0,0,0],
            [0,0,0,0,0,1,0,0,0,0],
            [0,0,0,0,0,0,1,0,0,0],
            [0,0,0,0,0,0,0,1,0,0],
            [0,0,0,0,0,0,0,0,1,0],
            [0,0,0,0,0,0,0,0,0,1]
        ]);

        Math.random = original;
    });

    it('should unveil empty cell on show', () => {
        instance.grid = testGrid;

        instance.show(0,3);

        expect(instance.grid[0][3]).toBe(CellValues.NOTHING_SHOWN);
    });

    it('should end game when showing mine cell', () => {
        instance.grid = testGrid;

        instance.show(0,0);

        expect(instance.gameOver).toBeTruthy();
    });

    it('should display mine number around selected cell', () => {
        instance.grid = testGrid;

        expect(instance.mineAround(0,1)).toBe(2);
        expect(instance.mineAround(3,2)).toBe(3);
        expect(instance.mineAround(9,8)).toBe(1);
        expect(instance.mineAround(0,4)).toBe(0);
    });

});