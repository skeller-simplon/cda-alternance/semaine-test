import { MineSweeper } from "../../src/game/MineSweeper";

describe('class MineSweeper', () => {
    let instance: MineSweeper;

    beforeEach(() => {
        instance = new MineSweeper();
    });

    it('Should instanciate a MineSweeper', () => {
        expect(instance).toBeTruthy();
    });

    it('Should have a grid property', () => {
        let grid = instance.grid;

        expect(grid).toEqual(null)
    })

    it('Should have a bomb symbol property', () => {
        let bombSymbol = instance.BOMB_SYMBOL;

        expect(bombSymbol).toEqual("X")
    })

    it('Should have a init grid function', () => {
        instance.initGrid(5, 5);

        let grid = instance.grid;

        expect(grid.length).toEqual(5)
        expect(grid[0].length).toEqual(5)
    })

    it('Shoud have at least a bombSymbol in each line of the grid', () => {
        instance.initGrid(5, 5)
        let grid = instance.grid;
        for (let index = 0; index < grid.length; index++) {
            const isBomb: boolean = grid[index].includes(instance.BOMB_SYMBOL)
            expect(isBomb).toBeTruthy()

        }
    })
    it('Shoud have a bombSymbol or a number or null on each box of the grid', () => {
        instance.initGrid(5, 5)
        let grid = instance.grid;
        for (let indexX = 0; indexX < grid.length; indexX++) {

            // Boucler sur grid[indexX]
            for (let indexY = 0; indexY < grid[indexX].length; indexY++) {
                const isBomb: boolean = grid[indexX][indexY] === instance.BOMB_SYMBOL
                const isEmpty: boolean = grid[indexX][indexY] === null
                const isNumber: boolean = typeof grid[indexX][indexY] === Number

            }
        }
    })
})


// [
//      [undefined, undefined, undefined, undefined, undefined]
//      [undefined, undefined, undefined, undefined, undefined]
//      [undefined, undefined, undefined, undefined, undefined]
//      [undefined, undefined, undefined, undefined, undefined]
//      [undefined, undefined, undefined, undefined, undefined]
// ]