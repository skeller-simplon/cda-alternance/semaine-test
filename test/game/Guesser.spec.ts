import { Guesser } from "../../src/game/Guesser"


describe('class Guesser', () => {

    let instance:Guesser;
    let originalLog = console.log;

    beforeEach(() => {
        instance = new Guesser(3);
    });

    afterEach(() => {
        console.log = originalLog;
    });


    it('should return true when right guess', () => {
        expect(instance.isAnswer('3')).toBeTruthy();
    });

    it('should call console log with "yes" on right guess', () => {
        const logMock = jest.fn();

        console.log = logMock;

        instance.handleInput('3');

        expect(logMock).toHaveBeenCalledWith('yes');

    });

    it('should call console log with "no" on wrong guess and call guess function', () => {
        const logMock = jest.fn();
        const guessMock = jest.fn();
        
        instance.guess = guessMock;
        console.log = logMock;

        instance.handleInput('2');

        expect(logMock).toHaveBeenCalledWith('no');

        expect(instance.guess).toHaveBeenCalled();

    });
})