import { TicTacToe } from "../../src/game/TicTacToe";

describe('class TicTacToe', () => { 
    let instance:TicTacToe;

    beforeEach(() => {
        instance = new TicTacToe();
    });

    it('Should instanciate a TicTacToe', () => {
        expect(instance).toBeTruthy();
    });
    
    it('Should return game grid', () => {
        const grid = instance.grid;

        expect(grid).toEqual([
            [null, null, null],
            [null, null, null],
            [null, null, null]
        ]);
    });

    it('Should start with O player', () => {
        const player = instance.currentPlayer;

        expect(player).toEqual('O');
    });

    it('Should switch to X after play', () => {
        instance.play(0,0);

        expect(instance.currentPlayer).toEqual('X');
    });

    it('Should alternate player on play', () => {
        instance.play(0,0);

        expect(instance.currentPlayer).toEqual('X');
        instance.play(0,1);

        expect(instance.currentPlayer).toEqual('O');
        instance.play(0,2);

        expect(instance.currentPlayer).toEqual('X');

    });

    it('Should place current player symbol on given coordinates', () => {

        instance.play(1,2);

        
        expect(instance.grid[1][2]).toEqual('O')

    });



});