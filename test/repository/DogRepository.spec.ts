import { Dog } from "../../src/entity/Dog";
import { connection } from "../../src/repository/connection";
import { DogRepository } from "../../src/repository/DogRepository";

describe('class DogRepository', () => {
    let instance:DogRepository;

    beforeEach(() => {
        instance = new DogRepository();
    });


    beforeEach(async () => {
        await connection.query('START TRANSACTION');
    });

    afterEach(async () => {
        await connection.query('ROLLBACK');
    });

    it('should return a Dog list', async () => {

        const results = await instance.findAll();

        expect(results.length).toBe(2);
        expect(results[0]).toBeInstanceOf(Dog);
        expect(results[0].name).toBe('Fido');

    });

    it('should add a new dog', async () => {
        await instance.add(new Dog('Test', 'Test', 1));
        const results = await instance.countDog();

        expect(results).toBe(3);

    });

})