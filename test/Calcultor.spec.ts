import { Calculator } from "../src/Calculator";



describe('class Calculator', () => {

    let instance: Calculator;

    beforeEach(() => {
        instance = new Calculator();
    });

    describe("Verify beforeEach instanciation", () => {
        it("Verify beforeEach instanciation, base", () => {
            instance["test"] = "test"
            expect(instance["test"]).toEqual("test");
        })
        it("Verify beforeEach instanciation, test", () => {
            expect(instance["test"]).toEqual(undefined);
        })
    })

    test.each([
        [1, 1, 2],
        [1, 2, 3],
        [2, 7, 9],
        [4, 2, 6],
        [-10, 100, 90],
        [60, 14, 74],
        // Cas limite js
        [Number.MAX_VALUE, Number.MAX_VALUE, Infinity]
    ])('.calculate(%i, %i, "+")', (a, b, expected) => {
        expect(instance.calculate(a, b, "+")).toEqual(expected)
    });

    test.each([
        [1, 1, 18],
        [1, 2, 30],
        [2, 7, -100],
        [4, 2, 5],
        [-10, 100, -90],
        [60, 14, -4],
        // Cas limite js
        [Number.MAX_VALUE, Number.MAX_VALUE, NaN]
    ])('.calculate(%i, %i, "+")', (a, b, expected) => {
        expect(instance.calculate(a, b, "+")).not.toEqual(expected)
    });

    test.each([
        [1, 1, 1],
        [1, 2, 2],
        [2, 7, 14],
        [4, 2, 8],
        [-10, 100, -1000],
        [60, 14, 840],
    ])('.calculate(%i, %i, "*")', (a, b, expected) => {
        expect(instance.calculate(a, b, "*")).toEqual(expected)
    });
    test.each([
        [1, 1, 0],
        [1, 2, 7],
        [2, 7, 9],
        [4, 2, 2],
        [-10, 100, -10],
        [60, 14, 2],
    ])('.calculate(%i, %i, "*")', (a, b, expected) => {
        expect(instance.calculate(a, b, "*")).not.toEqual(expected)
    });

    it('should perform addition', () => {

        const result = instance.calculate(2, 2, '+');
        expect(result).toEqual(4);
        

    });

    it('should perform substraction', () => {

        const result = instance.calculate(2, 2, '-');
        expect(result).toEqual(0);

    });

    it('should perform multiply', () => {

        const result = instance.calculate(2, 3, '*');
        expect(result).toEqual(6);

    });

    it('should perform division', () => {

        const result = instance.calculate(2, 2, '/');
        expect(result).toEqual(1);

    });

    it('should throw error when division by zero', () => {

        expect(() => instance.calculate(1, 0, '/')).toThrowError();
    });

    it('should throw error when unknown operator', () => {

        expect(() => instance.calculate(1, 0, 'x')).toThrowError('Invalid operator');
    });

    it('should throw error when a or b is not a number', () => {
        const a: any = 'jambon';

        expect(() => instance.calculate(a, 10, '*')).toThrowError('a and b must be numbers');

    })

    it.each([
        [1, 1, '+', 2],
        [1, 1, '-', 0],
        [1, 1, '*', 1],
        [4, 2, '/', 2],
        [0.3, 1, '+', 1.3],
    ])('should perform operation with a = %d b = %d operator = %s', (a, b, operator, result) => {
        expect(instance.calculate(a, b, operator)).toEqual(result)
    })
});