import * as readline from 'readline';

export class Guesser {

    constructor(private toGuess = Math.floor(Math.random() * 11)){}

    guess() {
        const rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });
        
        rl.question('What number am I thinking of ?', () =>this.handleInput);
    }


    
    handleInput(input: string) {
        if (this.isAnswer(input)) {
            console.log('yes');
        } else {
            console.log('no');
            this.guess();
        }
    }

    isAnswer(input: string):boolean {
        return Number(input) == this.toGuess;
    }
}