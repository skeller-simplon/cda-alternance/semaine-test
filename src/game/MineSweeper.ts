

export enum CellValues {
    NOTHING_HIDDEN,
    MINE_HIDDEN,
    NOTHING_SHOWN,
    MINE_SHOWN,
    NOTHING_FLAG,
    MINE_FLAG
}

export class MineSweeper {
    gameOver = false;
    grid: any[][];
    constructor(private size = 10) {
        this.generateGrid(size);
    }

    private generateGrid(size: number) {
        this.grid = [];
        for (let x = 0; x < size; x++) {
            this.grid[x] = new Array(size);
            this.grid[x].fill(CellValues.NOTHING_HIDDEN);
        }

        for (let i = 0; i < size; i++) {
            const x = Math.floor(Math.random() * 10);
            const y = Math.floor(Math.random() * 10);
            // console.log('x : '+x+'y : '+y);
            this.grid[x][y] = CellValues.MINE_HIDDEN;
        }



    }

    show(x: number, y: number) {
        if (!this.gameOver) {
            if (this.grid[x][y] === CellValues.MINE_HIDDEN) {

                this.grid[x][y] = CellValues.MINE_SHOWN;
                this.gameOver = true;
            }
            this.grid[x][y] = CellValues.NOTHING_SHOWN;
        }
    }

    mineAround(x: number, y: number): number {
        let mineCount = 0;
        for (let i = -1; i < 2; i++) {
            for (let j = -1; j < 2; j++) {
                if (i === 0 && j === 0) {
                    continue;
                }
                if ((this.grid[x + i] && this.grid[x + i][y + j]) && (this.grid[x + i][y + j] === CellValues.MINE_HIDDEN || this.grid[x + i][y + j] === CellValues.MINE_FLAG)) {
                    mineCount++;
                }
            }
        }
        return mineCount;
    }
}
