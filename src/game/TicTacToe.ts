

export class TicTacToe {
    

    constructor(
        public grid:string[][] = [
        [null, null, null],
        [null, null, null],
        [null, null, null]
    ],
    public currentPlayer = 'O'
    ) {}


    play(x:number,y:number) {

        this.grid[x][y] = this.currentPlayer;

        this.currentPlayer = this.currentPlayer === 'X' ? 'O':'X';
    }

}