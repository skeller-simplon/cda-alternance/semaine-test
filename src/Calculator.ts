


export class Calculator {

    calculate(a: number, b: number, operator: string) {
        if (isNaN(a) || isNaN(b)) {
            throw new Error('a and b must be numbers');
        }
        switch (operator) {
            case '+':
                return a + b;
            case '-':
                return a - b;
            case '/':
                if (b === 0) {
                    throw new Error('No division by zero');
                }
                return a / b;
            case '*':
                return a * b;
        }
        throw new Error('Invalid operator');
    }
}