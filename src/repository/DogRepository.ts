import { RowDataPacket } from "mysql2";
import { Dog } from "../entity/Dog";
import { connection } from "./connection";

export class DogRepository {
    async findAll():Promise<Dog[]> {
        const [result] = await connection.query<RowDataPacket[]>('SELECT * FROM dog');
        return result.map(row => new Dog(row['name'], row['breed'], row['age'], row['id']));
    }

    async add(dog: Dog) {
        await connection.query('INSERT INTO dog (name,breed,age) VALUE (?,?,?)', [
            dog.name,
            dog.breed,
            dog.age
        ]);
    }


    async countDog():Promise<number> {
        const [result] = await connection.query<RowDataPacket[]>('SELECT COUNT(*) as count FROM dog');

        return result[0]['count'];
    }

}