import * as readline from 'readline';

export class Guesser {

    constructor(public toGuess = Math.floor(Math.random() * 11)) { }

    guess() {
        var rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });

        rl.question('What number am I thinking of ?', input => {
            let ret = this.checkNumberMatch(Number(input));

            console.log(ret)
            if (ret === "no")
                this.guess();
        });

    }

    public checkNumberMatch(input: number) {
        if (input == this.toGuess) {
            return 'yes';
        } else {
            return 'no';
            this.guess();
        }
    }
}