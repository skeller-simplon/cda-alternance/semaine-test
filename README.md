# Semaine Test

Pour que les tests d'intégrations passent, faire en sorte d'importer la bdd via le db.sql et de configurer le .env.test avec les informations de connexion à la bdd.

Il est possible qu'il faille faire un `npm i esbuild -D` si aucun test ne passe